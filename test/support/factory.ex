defmodule Rumbl.Factory do
  use ExMachina.Ecto, repo: Rumbl.Repo

  def factory(:video) do
    %Rumbl.Video{
      title: sequence(:title, &"Factory created video #{&1}"),
      description: "A factory creted this video",
      url: "http://www.factory.com",
    }
  end

  def factory(:drama), do: %Rumbl.Category{ name: "drama"}
  def factory(:action), do: %Rumbl.Category{ name: "action"}
end
