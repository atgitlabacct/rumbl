defmodule Rumble.VideoMetricsTest do
  use ExUnit.Case, async: true

  alias Rumbl.VideoMetrics, as: VM

  test "#categories - list of empty videos generates an empty map" do
    assert VM.categories([]) == %{}
  end

  test "#categories - returns correct number for category" do
    drama = Rumbl.Factory.build(:drama)
    action = Rumbl.Factory.build(:action)

    videos = Rumbl.Factory.build_list(3, :video, [category: action])
    videos = [ Rumbl.Factory.build(:video, [category: drama]) | videos ]
    assert VM.categories(videos) == %{drama: 1, action: 3}
  end
end
