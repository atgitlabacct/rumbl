defmodule Rumbl.InfoSys.Supervisor do
  use Supervisor

  def start_link(), do: Supervisor.start_link(__MODULE__, [], name: __MODULE__)

  def init(_opts) do
    children = [
      # Restart temporary simply mean that the supervisor does not reload
      # the process in the case of failure
      worker(Rumbl.InfoSys, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end
