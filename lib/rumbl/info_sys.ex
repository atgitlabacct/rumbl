defmodule Rumbl.InfoSys do
  @backends [Rumbl.InfoSys.Wolfram]

  defmodule Result do
    defstruct score: 0, text: nil, url: nil, backend: nil
  end

  def start_link(backend, query, query_ref, owner, limit) do
    # Returns {:ok, pid} so that the supervisor can supervise
    backend.start_link(query, query_ref, owner, limit)
  end

  def compute(query, opts \\ []) do
    limit = opts[:limit] || 10
    # For each backend lets start a process (spawn_query) and add it to
    # our supervision tree.  We then with for a response within the timeout
    # period that was given
    @backends
    |> Enum.map(&spawn_query(&1, query, limit))
    |> await_results(opts)
    |> Enum.sort(&(&1.score >= &2.score))
    |> Enum.take(limit)
  end

  defp spawn_query(backend, query, limit) do
    query_ref = make_ref();
    opts = [backend, query, query_ref, self(), limit]

    # Now lets add a worker to the supervisor dynamically with start_child
    # this will call the start_link function from above.
    {:ok, pid} = Supervisor.start_child(Rumbl.InfoSys.Supervisor, opts)

    # Now setup a monitor for the backend.  If the pid goes down
    # we will get a message of DOWN
    monitor_ref = Process.monitor(pid)
    {pid, monitor_ref, query_ref}
  end

  defp await_results(children, opts) do
    timeout = opts[:timeout] || 9000
    # Lets wait for a given time period and if nothing is received we
    # send timedout message
    timer = Process.send_after(self(), :timedout, timeout)
    results = await_result(children, [], :infinity)

    # After a timeout or all messages received we cleanup
    cleanup(timer)
    results
  end

  defp await_result([head | tail], acc, timeout) do
    {pid, monitor_ref, query_ref} = head

    receive do
      {:results, ^query_ref, results} ->
        # We obvisouly recieved results so demonitor the process.
        Process.demonitor(monitor_ref, [:flush])
        await_result(tail, results ++ acc, timeout)
      {:DOWN, ^monitor_ref, :process, ^pid, _reason} ->
        await_result(tail, acc, timeout)
      :timedout ->
        kill(pid, monitor_ref)
        await_result(tail, acc, 0)
    after
      timeout ->
        kill(pid, monitor_ref)
        await_result(tail, acc, 0)
    end
  end

  defp await_result([], acc, _), do: acc

  defp kill(pid, ref) do
    Process.demonitor(ref, [:flush])
    Process.exit(pid, :kill)
  end

  defp cleanup(timer) do
    :erlang.cancel_timer(timer)
    receive do
      :timeout -> :ok
    after
      0 -> :ok
    end
  end
end
