defmodule Rumbl.VideoMetrics do
  @doc """
  Returns a map of categories and the count of times they appear in the
  videos list.

  ## Examples
  iex> VideoMetrics.categories(videos)
  ${drama: 10, action: 2, comedy: 3}
  """
  def categories([]), do: %{}
  def categories(videos) do
    Enum.reduce(videos, %{}, fn(video, acc) ->
      cat_name = String.to_atom(video.category.name)
      count = get_category_count(acc, cat_name)
      Map.put(acc, cat_name, count)
    end)
  end

  defp get_category_count(map, name) do
      if Map.has_key?(map, name), 
        do: Map.get(map, name) + 1,
        else: 1
  end
end
